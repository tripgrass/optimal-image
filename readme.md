# Optimal Image 
_A Plugin for Responsive Image and Lazy Loading_

## How to Use
  * Requires jQuery for lazy loading. Works error free without jQuery, but will not lazy-load.
  * Include (enqueue or compiled) [Waypoints.js](http://imakewebthings.com/waypoints/) to use the 'inView' lazy-load option. If 'inView' is called and Waypoints is not included, the plugin will fallback to lazy loading the image on jQuery(document).ready. 
  * Set the breakpoint width in pixels, for example to 700, in functions.php (breakpoint will default to 480):
  
  
		add_filter( 'optimal_image/breakpoint', function(){ return 700; } );

	
* Call the optimal image like so in place of the **<img>** element (from your template file):

		<?php 
			$args = array ( 
				'post'	  => $post,
				'mobile'  => 'small',	// use any media image size name (custom or WordPress default)
				'desktop' => 'large'
			);
			optimal_image( $args );
		?>
			
Optimal Image will output your img element like so (after the javascript runs):
	
	<img src="/correct-img-for-this-device.jpg" 
		height="correct-height" 
		width="correct-height"
		alt="alt">
##Features
  * The id _optimal_image_i_ is added to the **img** element, where _i_ is incremented for each image.
  * The class of _oi_image_ is automatically added to the **img** element.
  * The class _loaded_ is added to the **img** element if/when the image resource has loaded.
  * The class _loaded_ is added to the first _oi_image_ parent container (up to 3 above) that has a class of _oi_parent_.
  * Add your classes to the **img** element by using the space-separated list in the _classes_ argument like so: 'classes' => ' classOne classTwo'
  * Adds an **img** with an src wrapped in a **noscript** for disable javascript and bot image indexing. 	
##An Example
### Using ACF
	<?php
	
	/*
	 * Define an ACF field
	 * Use:
	  *	'type' => 'image',
	  *	'return_format' => 'array' 
	  * 
	  * This example uses an acf field named 'banner_image'
	*/
	if ( $banner = get_field('banner_image') ){
		$args = array( 
			'image' 	=> $banner,
			'mobile' 	=> 'small',
			'desktop'	=> 'large_banner'
		);
		optimal_image( $args ); 
	} 
	?>
#### Output ( for Using ACF example )
	<img
		id="optimal_image_1" 
		data-image-sizes= "{
			'small': { 
				'src': '/wp-content/uploads/test.jpg', 
				'width':'200','height':'400'
			},
			'large_banner': {
				'src': '/wp-content/uploads/test_large.jpg',
				'width':'400','height':'800'
			},
			'mobile':'small',
			'desktop':'large_banner'
		}">
	<noscript>
		<img 
			class="loaded no_script oi_image "
			id="optimal_image_1" 
			src="/wp-content/uploads/test_orig.jpg" 
			width="486" 
			height="324" 
			alt="original_alt_text">
	</noscript>
	<script> set_src('optimal_image_1'); </script>
   
    
##Other Examples
	
### Using a Post's Featured Image
	<?php
		if( have_posts() ) : while( have_posts ) : the_posts();
			$args = array( 
				'post' => $post,
			); 
			optimal_image( $args );
		endwhile; endif; 
	?>
### Using a Hardcoded Image URL
_Width and Height must be set or they will default to 0 and the image will not show_

    <?php 
    	$args = array( 
			'image' => get_template_directory_uri() . '/img/mobile.png',
			'width' => '800',
			'height' => '60',
			'desktop_image'	=> get_template_directory_uri() . '/img/desk.png',
			'desktop_width' => '1200',
			'desktop_height' => '60',
			'alt'	=> 'This is Alt Text'
		);
		optimal_image( $args ); ?>
    ?>

###Outputting a Background Image (using ACF)
	<?php 
	if ( $banner = get_field('banner_image') ){
		$args = array( 
			'image' 	=> $banner,
			'background' 	=> true,
			'mobile' 	=> 'small',
			'desktop'	=> 'large_banner'
		); 
	}
	?>
	<div class="some-class" <?php optimal_image( $args ); ?> >
	
Background images are currently output as an attribute within a div element. This limits the ability to inline javascript and requires background images to be lazyloaded. 
###Output for Background Image
	<div class="some-class" data-oi-lazy-load="true" id="optimal_image_1" data-oi-background="true" data-image-sizes="{'small': { 'src': '/wp-content/uploads/2015/05/test.jpg', 'width':'200','height':'400'} , 'large_banner': {'src': '/wp-content/uploads/2015/05/test_large.jpg' , 'width':'400','height':'800'} , 'mobile':'small' , 'desktop':'large_banner'}" ></div>
##Parameters

**$image**

  * (_string|array_)(_optional_) STRING of image src path OR ARRAY of ACF image array
  * Default: null

**$post**

  * (_object_)(_optional_) POST OBJECT, will grab the post's featured image
  * Default: null

**$mobile**

  * (_string_)(_optional_) The image size identifier for images to be loaded for mobile (screen widths less than or equal to the breakpoint) (i.e., 'full' or 'small'). Accepts WordPress default sizes or custom size as added in add_image_size
  * Default: null

**$desktop**

  * (_string_)(_optional_) The image size identifier for images to be loaded for desktop (screen widths greater less than the breakpoint) (i.e., 'full' or 'small'). Accepts WordPress default sizes or custom size as added in add_image_size
  * Default: $mobile

**$lazy**

  * (_string_)(_optional_) Use **load** or **inView**. When set, the plugin will lazy load the image on document ready ('load') or if WayPoints is included, will load when in view ('inView').
  * Default: null
  
**$classes**

  * (_string_)(_optional_)  String of space separated class names to be added to the image element like **'classOne classTwo'**.
  * Default: null
   
**$title**

  * (_string_)(_optional_) String of image title. This will override an existing image title (in the media post data). 
  * Default: $attachment_post->post_title
   
**$description**

  * (_string_)(_optional_) String of image description. This will override an existing image description (in the media post data). 
  * Default: $attachment_post->post_content
   
**$caption**

  * (_string_)(_optional_) String of image caption. This will override an existing image caption (in the media post data). 
  * Default: $attachment_post->post_excerpt
   
**$alt**

  * (_string_)(_optional_) String of image alt. This will override an existing image alt (in the media post data). 
  * Default: $attachment_post: post_meta meta_key = _wp_attachment_image_alt
   
**$width**

  * (_string_)(_optional_) String of image width. This will override an existing image  (in the media post data). 
  * Default: $attachment_post: post_meta meta_key= _wp_attachment_metadata
   
**$height**

  * (_string_)(_optional_) String of image height. This will override an existing image height (in the media post data). 
  * Default: $attachment_post: post_meta meta_key= _wp_attachment_metadata
      
**$background**

  * (_boolean_)(_optional_) If true, will output the included image element as a background image within a div element and will lazy load. 
  * Default: false
  

###A Little Explanation
	
  * The Javascript function definition for set_src() is output once, for the first optimal_image called. 
  * The **img** element is output with no src.
  * set_src('the_image_id') is called inline immediately after each **img** element. Based on the viewport width, optimal_image() sets the src to the correct url (declared in the data-image-sizes).
  * If the **img** is to be lazy loaded, a jquery function is output to the footer and called on doc.ready.
  * The **optimal_image img (with no src)**  is followed by an img element with the full size original image as an src wrapped in a noscript. This provides a fallback for when javascript is disabled and allows the img to be indexed by search engines.