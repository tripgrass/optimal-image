<?php
	
	if ( ! defined( 'ABSPATH' ) ) exit;

	class optimal_image {
	
		/**
		 * The single instance of optimal_image.
		 * @var 	object
		 * @access  private
		 * @since 	0.0.1
		 */
		private static $_instance = null;
	
		/**
		 * Settings class object
		 * @var     object
		 * @access  public
		 * @since   0.0.1
		 */
		public $settings = null;
	
		/**
		 * The token.
		 * @var     string
		 * @access  public
		 * @since   0.0.1
		 */
		public $_token;
	
		/**
		 * The main plugin file.
		 * @var     string
		 * @access  public
		 * @since   0.0.1
		 */
		public $file;
	
		/**
		 * The main plugin directory.
		 * @var     string
		 * @access  public
		 * @since   0.0.1
		 */
		public $dir;
	
		/**
		 * The plugin assets directory.
		 * @var     string
		 * @access  public
		 * @since   0.0.1
		 */
		public $assets_dir;
	
		/**
		 * The plugin assets URL.
		 * @var     string
		 * @access  public
		 * @since   0.0.1
		 */
		public $assets_url;
		
		/**
		 * Breakpoint 
		 * @var     string
		 * @access  public
		 * @since   0.0.1
		 */
		public $breakpoint;

		/**
		 * Constructor function.
		 * @access  public
		 * @since   0.0.1
		 * @return  void
		 */
		public function __construct ( $file = '', $version = '0.0.1' , $options ) {
			$this->_version = $version;
			$this->_token = 'optimal_image';

			// Load plugin environment variables
			$this->file = $file;
			$this->dir = dirname( $this->file );
			$this->assets_dir = trailingslashit( $this->dir ) . 'assets';
			$this->assets_url = esc_url( trailingslashit( plugins_url( '/assets/', $this->file ) ) );

			// database prefix:	
			$this->base = $options['base'];
			$this->breakpoint = $options['breakpoint'];
			register_activation_hook( $this->file, array( $this, 'install' ) );
		} // End __construct ()

		/**
		 * Build Optimal Image
		 * @return 
		 */
		public function build_optimal_image( $args ) {
			$script = ""; // var to output inline functions
			$this->args = $args;		
			$sizes_acf = get_intermediate_image_sizes();	// get all image sizes and add to array
			if(is_array($sizes_acf)){
				foreach($sizes_acf as $size){
					$this->sizes[$size] = $size;
				}
			}
	
			// if there's no desktop size set, use mobile for desktop
			if( !array_key_exists( 'desktop', $this->args ) && array_key_exists( 'mobile', $this->args )){
				$this->args['desktop'] = $this->args['mobile'];
			}
			if ( is_array( $this->args ) ) :
				$defaults = array(
		            'mobile' 		=> 'original',
		            'desktop'  		=> 'original',
		            'lazy'  		=> false,
		            'classes'  		=> '',
					'title' 		=> '',
					'description' 	=> '',
					'caption' 		=> '',
					'alt' 			=> '',
					'width' 		=> '',
					'height' 		=> '',
					'background' 	=> false,
					'desktop_width' => $this->breakpoint
		        );
	
				// check if an image
				if(  (!array_key_exists( 'image',  $this->args ) || !$this->args['image'] ) &&  (!array_key_exists( 'post',$this->args) || !$this->args['post'] ) ){
					$this->args['image'] = $this->assets_url.'img/nothing.gif';
				}
				$img = array();
		        $args = wp_parse_args( $this->args, $defaults );
		 		extract( $args, EXTR_SKIP );
				if( array_key_exists( 'image' , $args ) || array_key_exists( 'post' , $args ) ){
					if( array_key_exists( 'post' , $args ) && is_object( $args['post'] ) && array_key_exists( 'post_type' , $args['post']) ){ // is a post and will setup featured image
						//  sets up the $img array like the acf image object 
						$postObj = $args['post'];
	
						$img_id = get_post_thumbnail_id( $postObj->ID );
						if( !$img_id ){
							$image = $args['image'] = $this->assets_url.'img/nothing.gif';
						}
						else{
							$img_data = wp_get_attachment_metadata( $img_id);
							$img_post = get_post($img_id);
							$img['title'] = $img_post->post_title;
							$img['url'] = $img_post->guid;
							$img['description'] = $img_post->post_content;
							$img['caption'] = $img_post->post_excerpt;
							$img['alt'] = get_post_meta($img_id, '_wp_attachment_image_alt', true);
							$img['width'] = $img_data['width'];
							$img['height'] = $img_data['height'];
							if( array_key_exists( $mobile , $img_data['sizes'] ) ){
								$mobile_arr = wp_get_attachment_image_src( $img_id , $mobile );
								$img['sizes'][$mobile] = $mobile_arr[0];
								$img['sizes'][$mobile.'-width'] = $img_data['sizes'][$mobile]['width'];
								$img['sizes'][$mobile.'-height'] = $img_data['sizes'][$mobile]['height'];
							}
							if( array_key_exists( $desktop , $img_data['sizes'] ) ){
								$desktop_arr = wp_get_attachment_image_src( $img_id , $desktop );
								$img['sizes'][$desktop] = $desktop_arr[0];
								$img['sizes'][$desktop.'-width'] = $img_data['sizes'][$desktop]['width'];
								$img['sizes'][$desktop.'-height'] = $img_data['sizes'][$desktop]['height'];
							}
						}
					}
					if( array_key_exists( 'image' , $args ) && is_array( $args['image'] ) ){
						if( array_key_exists( 'type' , $args['image'] ) && 'image' ==  $args['image']['type']){
							$img = $args['image'];
						}
						$load = true;
					}
					elseif( array_key_exists( 'image' , $args ) && is_string( $args['image'] ) ){ // is a hardcoded image, will setup $img from a string url (like: /assets/img/this.png )
						$img['title'] = $title;
						$img['url'] = $image;
						$img['description'] = $description;
						$img['caption'] = $caption;
						$img['alt'] = $alt;
						$img['width'] = $width;
						$img['height'] = $height;
						$img['mobile'] = 'original';
						$img['desktop'] = 'desktop';
						if( array_key_exists( 'desktop_image' , $args ) && is_string( $args['desktop_image'] ) ){
							$args['desktop'] = 'desktop';
							$img['sizes']['original'] = $image;
							$img['sizes']['original-width'] = $width;
							$img['sizes']['original-height'] = $height;
							$img['sizes']['desktop_image'] = $desktopSrc = $args['desktop_image'];
							$img['sizes']['desktop_image-width'] = $desktopWidth = $args['desktop_width'];
							$img['sizes']['desktop_image-height'] = $desktopHeight = $args['desktop_height'];
							$data_attributes['desktop_image'] = array("src"=>$desktopSrc,"width"=> $desktopWidth, "height"=>$desktopHeight);
							$desktop = 'desktop_image';
						}
					}
				}
				elseif( array_key_exists( 'type' , $args) && $args['type'] == 'image' ){ // is an acf array
					$img = $args;
					$type = 'original';	
					$load = true;
				}
				// verify there is an $img array setup

				if( is_array( $img ) and count( $img ) > 0 ):
					// If this is the first time optimal_image is called on a page load, we'll add a counter to $GLOBALS['optimal_image_id'] for image unique ids
					if( !array_key_exists( 'optimal_image_id', $GLOBALS ) ): 
						// we'll add the jquery functions for lazy-loading the necessary images
						add_action( 'wp_footer', function(){
							$footerOI = optimal_image_main();
							$footerOI->oi_jquery();
						} , 10000);
						// next we'll output the necessary inline js functions: 
					
						$script = "					
							<script>
								// inline script output from optimal-image.php; only printed when a page has an optimal-image img element
								var bp = ".$this->breakpoint.",
									oi_lazy = [],
									oi_lazyInView = [];
								bp_type = 'desktop';
								if( window.innerWidth <= bp ){
									bp_type = 'mobile';
								}
								// looks for class of oi_parent on image's parent; returns parent if no class found;
								function checkParent( el , i ){
									var thisParent = el.parentNode;
									if(i < 3){
										if ( (' ' + thisParent.className + ' ').indexOf(' oi_parent ') > -1 ){
											thisParent.className = thisParent.className + ' loaded';
											return true;
										}
										else{
											i++;
											checkParent( thisParent, i ); 
										}
									}
								}
								window.set_src = function( selector , after_load ){
									var el = document.querySelector( '#' + selector );
									if( el ){
										// add loaded class on image after it has loaded
										if (el.addEventListener) {
											el.addEventListener('load', function() { 
												el.className = el.className + ' loaded';
												checkParent(el, 0);
											});
										}
										else {
											el.attachEvent('load', function() { 
												el.className = el.className + ' loaded';
												checkParent(el, 0);
											});
										}
										var JSONattributes = el.getAttribute('data-image-sizes'),
											lazyLoad = el.getAttribute('data-oi-lazy-load'),
											background = el.getAttribute('data-oi-background'),
											lazyInview = el.getAttribute('data-oi-lazy-inview');
									}
									if( JSONattributes ){
										var attributes = JSON.parse( JSONattributes ),
											image_type = attributes[bp_type],
											thisImage = attributes[image_type];
											if( typeof thisImage != 'undefined'){
		
												el.width = thisImage.width;
												el.height = thisImage.height;
												var newUrl = thisImage.src;
											}
											else{
												var newUrl = attributes.original['src'];
											}
											if( !lazyLoad && !lazyInview || after_load){
												if( background ){
													el.style.backgroundImage = \"url('\" + newUrl + \"')\";
												}
												else{
													el.src = newUrl;
												}
											}
									}
								}
							</script>";
		
						$GLOBALS['optimal_image_id'] = 1;
					else:
						$GLOBALS['optimal_image_id']++;
					endif;
					$image_id = "optimal_image_" . $GLOBALS['optimal_image_id'];
					// takes an acf image array and outputs the img tag:
					$alt = $img['alt'];
					$title = $img['title'];
					$caption = $img['caption'];
					$width = $img['width'];
					$height = $img['height'];
					$src = $img['url'];
					$data_attributes['original'] = array("src"=>$src,"width"=> $width, "height"=>$height);
		 			$data_attributes['mobile'] = $mobile;
					$data_attributes['desktop'] = $desktop;
					foreach( $this->sizes as $size ){
						if ( array_key_exists( 'sizes', $img ) && ( $size == $mobile || $size == $desktop ) ){
							$thisSrc = $img['sizes'][$size];
							$thisWidth = $img['sizes'][$size . '-width'];
							$thisHeight = $img['sizes'][$size . '-height'];
							$data_attributes[ $size ] = array("src"=>$thisSrc, "width"=> $thisWidth, "height"=>$thisHeight);
						}
					}
					$data_lazy = "";
					if( $lazy || $background ){
						if( $lazy === "inView" ){
							$data_lazy = " data-oi-lazy-inview='true' ";
						}
						else{
							$data_lazy = " data-oi-lazy-load='true' ";
						}
					}
					if( $background ){
						$out = $data_lazy.' id="'.$image_id.'" data-oi-background="true" data-image-sizes=\''. json_encode( $data_attributes ) .'\'' ; 
					}
					else{
						echo $script;
						if( array_key_exists( $desktop, $data_attributes ) && array_key_exists( 'width', $data_attributes[ $desktop ] ) ){
							$width = $data_attributes[ $desktop ]['width'];
							$height = $data_attributes[ $desktop ]['height'];
						}
		
						$out = '<img class="oi_image '.$classes.'" id="'.$image_id.'" '.$data_lazy.' data-image-sizes=\''. json_encode( $data_attributes ) .'\' alt="'.$alt.'" width="'.$width.'" height="'.$height.'">'; 
						$out .= '<noscript><img class="loaded no_script oi_image '.$classes.'" id="'.$image_id.'" src="'.$src.'" width="'.$width.'" height="'.$height.'"alt="'.$alt.'"></noscript>'; 
					}
					echo $out; ?>
					<?php if( !$lazy && !$background): ?>
						<script>window.set_src( '<?php echo $image_id; ?>'  );</script>
					<?php endif; ?>
		
				<?php endif; // $img is not an array ?>
			<?php endif; // $args is not an array 
		}	
	// ADD jquery dependent script to footer
	public function oi_jquery() {
		if( wp_script_is( 'jquery', 'done' ) ) { ?>

	    <script type="text/javascript">
			if (window.jQuery) {  
				$breakpoint = <?php echo $this->breakpoint; ?>;
				jQuery(document).ready(function($) {
					function loadLazies(){
							var $lazies = $('[data-oi-lazy-load], [data-oi-lazy-inview]');
							//check if waypoint is loaded; if it isn't, just add the inview elements to the normal lazy load elements below
							if( jQuery().waypoint ) {
								var $scrollLazies = $('[data-oi-lazy-inview]');
								if( $($scrollLazies).length ){
									$($scrollLazies).each( function(){
										var $this = this;
										var waypoint = new Waypoint({
											element: $($this),
											handler: function(direction) {
												window.set_src( $this.id, true);
			 									this.destroy();
											},
											offset: '96%'
										})
									});
								}
								var $lazies = $('[data-oi-lazy-load]');
							}
							if( $($lazies).length ){
								$($lazies).each( function(){
									var $this = this;
									window.set_src( $this.id, true);
								});
							}
					}
					if( typeof window.set_src != 'function'){					
						$.getScript( "<?php echo $this->assets_url; ?>js/frontend.min.js", function( data, textStatus, jqxhr ) { 
							loadLazies(); 
						});					
					}
					else{
						loadLazies();
					}				
				});
			}
			else{
				console.log('jQuery is required for optimal-image');
			}
	    </script>

<?php }
	}
	
	
		/**
		 * Main optimal_image Instance
		 *
		 * Ensures only one instance of optimal_image is loaded or can be loaded.
		 *
		 * @since 0.0.1
		 * @static
		 * @see optimal_image()
		 * @return Main optimal_image instance
		 */
		public static function instance ( $file = '', $version = '0.0.1' , $options ) {
			if ( is_null( self::$_instance ) ) {
				self::$_instance = new self( $file, $version , $options );
			}
			return self::$_instance;
		} // End instance ()
	
		/**
		 * Installation. Runs on activation.
		 * @access  public
		 * @since   0.0.1
		 * @return  void
		 */
		public function install () {
			$this->_log_version_number();
		} // End install ()
	
		/**
		 * Log the plugin version number.
		 * @access  public
		 * @since   0.0.1
		 * @return  void
		 */
		private function _log_version_number () {
			update_option( $this->_token . '_version', $this->_version );
		} // End _log_version_number ()
	
	}